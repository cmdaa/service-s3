package io.cmdaa.s3.service.resource

import java.io.{ByteArrayInputStream, InputStream}

import io.cmdaa.s3.service.auth.AppUser
import io.minio.MinioClient
import javax.ws.rs.core.Response.Status
import org.joda.time.DateTime
import org.junit.runner.RunWith
import org.scalamock.scalatest.MockFactory
import org.scalatest.junit.JUnitRunner
import org.scalatest.{Assertion, BeforeAndAfter, FlatSpec}

import scala.util.matching.Regex


@RunWith(classOf[JUnitRunner])
class UploadResourceTest  extends FlatSpec with MockFactory with BeforeAndAfter {

   val client: MinioClient = new MinioClient("https://play.minio.io:9000","Q3AM3UQ867SPQQA43P2F","zuf+tfteSlswRu7BJ86wekitnifILbZam1KYY3TG")
 //  val client: MinioClient = new MinioClient("http://localhost:8080","Q3AM3UQ867SPQQA43P2F","zuf+tfteSlswRu7BJ86wekitnifILbZam1KYY3TG")


   val principal: AppUser = new AppUser(1L, "principal", Set("ADMIN"))
   val testIn: InputStream  = new ByteArrayInputStream("test data".getBytes("UTF-8"))
   val uuidPattern: Regex = """/^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$/""".r
   var fileNameMap: Map[String,String] = Map("fileName"->"test")
   var fileName: String = s""

  val bucketName: String = s"cmdaa"

  val found: Boolean = client.bucketExists(bucketName)

  if (!found) {
    client.makeBucket(bucketName)
  }

  val currentBucket: s3Bucket = new s3Bucket(client,bucketName)

  val newUpload: UploadResource = new UploadResource(currentBucket)


  "uploading a file to play.minio.io...need internet access " should "succeed" in {
    val resp = newUpload.uploadFile(principal, testIn, null, new DateTime().toDate.getTime)
    assert(Status.OK.getStatusCode === resp.getStatus)

    val retMap = resp.getEntity.asInstanceOf[Map[String, String]]
    //  println("test value is " + retMap("fileName"))
    //  println("return value is " + resp.getEntity())
    assert(uuidPattern.findFirstIn(retMap("fileName")) != Option(""))

    fileName = retMap("fileName")
  }

  "deleting a file to play.minio.io...need internet access " should "succeed" in {
    val resp2 = newUpload.deleteFile(principal,fileName)

    val retMap2 = resp2.getEntity.asInstanceOf[Map[String,String]]
    assert(Status.OK.getStatusCode === resp2.getStatus)
    assert(retMap2(fileName) === "REMOVED")
    //assert(retMap2(fileName) === "NOT FOUND")
  }

  "correctly parse S3 file URLs" should "succeed" in {
    parse_path("qwe/apport.log_1580221194624/")
    parse_path("qwe/apport_meow.log_1580221194624/")
    parse_path("qwe_1-23@mail.com/app-ort_m eow.log_1580221194624/")
  }

  def parse_path(logPath: String): Assertion = {
    logPath match {
      case newUpload.logPattern(username, logName, lastModifiedStr) =>
        println(s"U: $username, L: $logName, M: $lastModifiedStr")
        succeed
      case _ =>
        fail("LogPattern not found in: " + logPath)
    }
  }
}
