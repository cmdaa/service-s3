package io.cmdaa.s3.service.resource

import java.io.{ByteArrayInputStream, InputStream}
import java.util.UUID.randomUUID

import io.minio.{MinioClient, Result, messages}
import io.minio.messages.Item
import org.junit.runner.RunWith
import org.scalamock.scalatest.MockFactory
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfter, FlatSpec}
import java.io.InputStream

import akka.http.scaladsl.model
import akka.http.scaladsl.model.DateTime
import io.findify.s3mock.response
import io.findify.s3mock.response.Bucket

import scala.collection.JavaConverters._
import io.minio.messages.{Bucket, Item}
import org.joda.time.DateTime
import java.lang.Iterable




@RunWith(classOf[JUnitRunner])
class s3BucketTest  extends FlatSpec with MockFactory with BeforeAndAfter {


  /*Note: Minio does not seem to work well with a stub so I am using the minio public test site for
  this test.  Also, when I tried to stub the s3Bucket which depends on minio that would not work properly either
*/
  val client: MinioClient = new MinioClient("https://play.minio.io:9000","Q3AM3UQ867SPQQA43P2F","zuf+tfteSlswRu7BJ86wekitnifILbZam1KYY3TG")

  //val client: MinioClient = new MinioClient("http://localhost:8080","Q3AM3UQ867SPQQA43P2F","zuf+tfteSlswRu7BJ86wekitnifILbZam1KYY3TG")

  val fileName = randomUUID.toString

  val bucketName: String = s"cmdaa"

  val found: Boolean = client.bucketExists(bucketName)

  if (!found) {
    client.makeBucket(bucketName)
  }

  "uploading a file to play.minio.io...need internet access " should "succeed" in {

    val currentBucket: s3Bucket = new s3Bucket(client,bucketName)

    val testIn: InputStream  = new ByteArrayInputStream("test data".getBytes("UTF-8"))

    //Test the upload of a file when I know it should work
    val testStr: String = currentBucket.upload(fileName,testIn,false)
    println(s"testStr is $testStr")
    //Test the upload of a file with the same name as above...this should fail
    val testStr2: String = currentBucket.upload(fileName,testIn,false)
    assert(testStr === fileName)
    assert(testStr2 === "")
  }

  "Deleting a file at play.minio.io...need internet access " should "succeed" in {

    val currentBucket: s3Bucket = new s3Bucket(client,bucketName)
    //Create a UUID that we know is not a filename on the minio site to make sure when the delete fails
    //we get the correct result.
    val fakeFileName = randomUUID.toString

    //Test the delete when I know the file is there
    val testStr: String = currentBucket.delete(fileName)
    assert(testStr === "REMOVED")

    //Test the delete with a fake file name to make sure the failure responds correctly
    val testStr2: String = currentBucket.delete(fakeFileName)
    assert(testStr2 === "NOT FOUND")

  }



  /*  Saving this in case I can ever get MinioClient to work as a stub...can't get it to work now
  val client = mock[io.minio.MinioClient]
  val fileName = "testFileName.txt"

  val bucketName: String = s"cmdaa"
  val testEx: Exception = new Exception()
  val objList = new Result( new io.minio.messages.Item(),testEx).asInstanceOf[java.lang.Iterable[Result[Item]]]
  // val tmpResult: Result[Item] = io.minio.Result(new Item(fileName,false),testEx)

  val testIn: InputStream = new ByteArrayInputStream("test data".getBytes("UTF-8"))

  // val bucketList: Iterable[Result[String]] = Iterable(Result(tmpResult))
  // val objList = objIterable = List(tmpResult).asInstanceOf[java.lang.Iterable]

  (client.bucketExists _).when(bucketName).returns(true)
  (client.listObjects( _: String, _: String)).when(bucketName,fileName).returns(objList)
  (client.putObject(_: String, _: String, _: InputStream, _: String)).when(bucketName, fileName, testIn, "application/octet-stream").returns((): Unit)

  val currentBucket = stub[s3Bucket]

  val curBucket: s3Bucket = new s3Bucket(client, "fakeConfig")


  "s3Bucket.upload" should "return the name of the file" in {

    (curBucket.upload( _:String, _: InputStream, _: Boolean)).when(fileName,testIn,false).returns(fileName)

    val retVal: String = curBucket.upload(fileName,testIn,false)
    assert(retVal === "testFileName.txt")
  }
*/
}
