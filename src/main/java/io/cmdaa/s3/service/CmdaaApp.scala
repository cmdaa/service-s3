package io.cmdaa.s3.service

import com.fasterxml.jackson.databind.{ObjectMapper, SerializationFeature}
import com.fasterxml.jackson.datatype.joda.JodaModule
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.Logger
import io.cmdaa.data.dao.UserProjectRepo
import io.cmdaa.s3.service.auth.{AppAuthenticator, AppAuthorizer, AppUser}
import io.cmdaa.s3.service.resource.{AuthResource, RootResource, UploadResource, s3Bucket}
import io.dropwizard.Application
import io.dropwizard.auth.oauth.OAuthCredentialAuthFilter
import io.dropwizard.auth.{AuthDynamicFeature, AuthValueFactoryProvider}
import io.dropwizard.setup.Environment
import io.minio.MinioClient
import org.eclipse.jetty.servlets.CrossOriginFilter
import org.eclipse.jetty.servlets.CrossOriginFilter.{ALLOWED_HEADERS_PARAM, ALLOWED_METHODS_PARAM, ALLOWED_ORIGINS_PARAM}
import org.glassfish.jersey.media.multipart.MultiPartFeature
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature
import slick.jdbc.JdbcBackend.Database
import slick.jdbc.PostgresProfile

import javax.servlet.DispatcherType

class CmdaaApp() extends Application[CmdaaAppConfig] {
  val logger: Logger = Logger[CmdaaApp]

  override def getName: String = "cmda-service-s3"

  override def run(t: CmdaaAppConfig, env: Environment): Unit = {
    val db = Database.forConfig("postgresql")
    val userProjectRepo = new UserProjectRepo(PostgresProfile, db)

    val config = ConfigFactory.load()

    //val server = System.getenv("MINIO_SERVER")
    //val port = System.getenv("MINIO_PORT")
    val minioEndpoint = System.getenv("MINIO_ENDPOINT")
    val key = System.getenv("MINIO_KEY")
    val secret = System.getenv("MINIO_SECRET")
    val bucket = System.getenv("MINIO_BUCKET")

    println("minioEndpoint = " + minioEndpoint)
    println("key = " + key)
    println("secret = " + secret)
    println("bucket = " + bucket)
    //val s3Client: MinioClient = new MinioClient("http://" + config.getString("minio.cmdaa.server") + ":" + config.getString("minio.cmdaa.port"),
    //  config.getString("minio.cmdaa.key"),config.getString("minio.cmdaa.secret"))

    val s3Client: MinioClient = new MinioClient(minioEndpoint, key, secret)
    val curBucket: s3Bucket = new s3Bucket(s3Client, bucket)

    logger.info("Token Expire Time in ms = "  + t.accessTokenExpireTimeMillis)

    env.jersey().register(new RootResource)

    // Need this to make the file upload code work in UploadResource
    env.jersey().register(new MultiPartFeature)
    env.jersey().register(new UploadResource(curBucket))
    env.jersey().register(new AuthResource(userProjectRepo, t.allowedGrantTypes))
    env.jersey.register(jacksonJaxbJsonProvider)

    env.jersey().register(new AuthDynamicFeature(
      new OAuthCredentialAuthFilter.Builder()
        .setAuthenticator(new AppAuthenticator(userProjectRepo, t.accessTokenExpireTimeMillis))
        .setAuthorizer(new AppAuthorizer())
        .setPrefix("Bearer")
        .buildAuthFilter()))
    env.jersey.register(classOf[RolesAllowedDynamicFeature])
    env.jersey.register(new AuthValueFactoryProvider.Binder(classOf[AppUser]))

    // Enable CORS headers
    val cors = env.servlets.addFilter("CORS", classOf[CrossOriginFilter])

    // Configure CORS parameters
    cors.setInitParameter(ALLOWED_ORIGINS_PARAM, "*")
    cors.setInitParameter(ALLOWED_HEADERS_PARAM, "X-Requested-With,Content-Type,Accept,Origin,Authorization")
    cors.setInitParameter(ALLOWED_METHODS_PARAM, "OPTIONS,GET,PUT,POST,DELETE,HEAD")
    //logger.info("HERE")

    // Add URL mapping
    cors.addMappingForUrlPatterns(java.util.EnumSet.allOf(classOf[DispatcherType]), true, "/*")

    //env.servlets().addFilter("AuthServletFilter", classOf[AuthServletFilter])
    //  .addMappingForUrlPatterns(java.util.EnumSet.allOf(classOf[DispatcherType]), true, "/*")
  }

  private def jacksonJaxbJsonProvider: JacksonJaxbJsonProvider = {
    val provider = new JacksonJaxbJsonProvider()
    val objectMapper = new ObjectMapper()
    objectMapper.registerModule(DefaultScalaModule)
    objectMapper.registerModule(new JodaModule)
    objectMapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false)
    objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
    objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
    provider.setMapper(objectMapper)

    provider
  }
}

object CmdaaApp {
  def main(args: Array[String]): Unit =
    new CmdaaApp().run(args: _*)
}
