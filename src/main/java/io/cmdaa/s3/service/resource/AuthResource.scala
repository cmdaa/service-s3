package io.cmdaa.s3.service.resource

import java.security.cert.X509Certificate

import com.typesafe.scalalogging.Logger
import io.cmdaa.s3.service.util.PasswordStorage
import javax.naming.ldap.{LdapName, Rdn}
import javax.servlet.http.HttpServletRequest
import javax.ws.rs._
import javax.ws.rs.core.MediaType.APPLICATION_JSON
import javax.ws.rs.core.{Context, Response}
import io.cmdaa.data.dao.UserProjectRepo
import io.cmdaa.data.util.entity.UserSchema.{AccessToken, User}

import scala.collection.JavaConverters._

@Path("/oauth2/token")
@Produces(Array(APPLICATION_JSON))
class AuthResource (
  userProjectRepo: UserProjectRepo,
  allowedGrantTypes: Array[String]
) {
  private val logger: Logger = Logger[AuthResource]

  @POST
  @Consumes(Array(APPLICATION_JSON))
  def postForToken(in: Map[String, Any]): Response = {
    val grantType: String = in.getOrElse("grantType", "").toString
    val username:  String = in.getOrElse("username", "").toString
    val password:  String = in.getOrElse("password", "").toString

    // Check if the grant type is allowed
    if (allowedGrantTypes.contains(grantType)) {

      // Try to find a user with the supplied credentials
      val userOption: Option[User] = userProjectRepo.findUserByUsername(username)
      if (userOption.nonEmpty) {

        // Check password hashes
        val user: User = userOption.get
        if (PasswordStorage.verifyPassword(password, user.passwordHash)) {

          // User was found, delete any existing tokens for the user, generate
          // a new token and return it
          userProjectRepo.deleteAccessTokensForUser(user.id.get)
          val token: AccessToken = userProjectRepo.generateAccessToken(user.id.get)
          Response.ok.entity (
            Map (
              ("uuid", token.uuid.get),
              ("userId", token.userId),
              ("lastAccessedDateMillis", token.lastAccessDateMillis)
            )
          ).build()
        } else {
          Response.status(Response.Status.UNAUTHORIZED).build()
        }
      } else {
        Response.status(Response.Status.UNAUTHORIZED).build()
      }
    } else {
      Response.status(Response.Status.METHOD_NOT_ALLOWED).build()
    }
  }

  @GET
  @Consumes(Array(APPLICATION_JSON))
  def getForToken(@Context request: HttpServletRequest): Response = {
    val grantType: String = "x509"
    var username: String = ""

    val opt = Option(request.getAttribute("javax.servlet.request.X509Certificate"))

    if (opt.nonEmpty) {
      val certs: Array[X509Certificate] = opt.get.asInstanceOf[Array[X509Certificate]]

      for (cert <- certs) {
        val dn: String = cert.getSubjectDN.getName
        logger.info(s"====> DN: $dn")

        val ldapDN: LdapName = new LdapName(dn)

        for (rdn: Rdn <- ldapDN.getRdns.asScala.toSet) {
          if ("CN" == rdn.getType) {
            logger.info(s"====> Type: ${rdn.getType}, Value: ${rdn.getValue}")
            username = rdn.getValue.toString
          }
        }
      }
    }

    // Check if the grant type is allowed
    if (allowedGrantTypes.contains(grantType)) {
      // Try to find a user with the supplied credentials
      val userOption: Option[User] = userProjectRepo.findUserByUsername(username)

      if (userOption.nonEmpty) {
        // Check password hashes
        val user: User = userOption.get

        //if (PasswordStorage.verifyPassword(password, user.passwordHash)) {

        // User was found, delete any existing tokens for the user, generate a new token and return it
        userProjectRepo.deleteAccessTokensForUser(user.id.get)
        val token: AccessToken = userProjectRepo.generateAccessToken(user.id.get)
        Response.ok.entity (
          Map (
            ("uuid", token.uuid.get),
            ("userId", token.userId),
            ("lastAccessedDateMillis", token.lastAccessDateMillis)
          )
        ).build()

        //} else {

        //  Response.status(Response.Status.UNAUTHORIZED).build()
        //}
      } else {
        Response.status(Response.Status.UNAUTHORIZED).build()
      }
    } else {
      Response.status(Response.Status.METHOD_NOT_ALLOWED).build()
    }
  }
}
