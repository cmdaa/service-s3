package io.cmdaa.s3.service.resource

import java.io.{ByteArrayOutputStream, InputStream}

import io.minio.MinioClient
import io.minio.errors.MinioException

import scala.collection.JavaConverters._
import scala.util.Try

class s3Bucket(private val client: MinioClient, val name: String) {

  /**
   * Lists uploads by User.
   *
   * @param userName the name of the user who's file list to retrieve
   * @return a list of files
   */
  def listUploads(userName: String): List[String] =
    fetchChildDirs(userName)

  /**
   * Lists runs for the given upload path
   *
   * @param uploadPath the upload path to list runs for
   * @return a list of run paths
   */
  def listRuns(uploadPath: String): List[String] =
    fetchChildDirs(uploadPath)

  /**
   * Uploads a file to S3.
   *
   * @param objName    the name of the S3 Object to upload
   * @param dataStream the inputStream of data to upload
   * @param overwrite  whether or not to overwrite an existing S3 Object
   * @return
   */
  def upload(objName: String, dataStream: InputStream, overwrite: Boolean)
  : String = {
    try {
      // Check whether bucket with "name" exists or not.
      // If it does not exist create it.
      if (!client.bucketExists(name)) client.makeBucket(name)
    } catch { case e: MinioException =>
      println("Error occurred: " + e)
    }

    Try {
      if (!overwrite && client.listObjects(name, objName).asScala.size == 1)
        ""
      else {
        client.putObject(name, objName, dataStream, "application/octet-stream")
        objName
      }
    } getOrElse ""
  }

  /**
   * Fetches the given path from S3 returning the contents of the file as a
   * String.
   *
   * @param path the path of the file to fetch
   * @return the contents of the fetched file as a String
   */
  def fetch(path: String): Option[String] = {
    try {
      if (!client.bucketExists(name)) return None
    } catch { case mEx: MinioException =>
      println("Error occurred: " + mEx)
      return None
    }

    Try {
      istream2str(client.getObject(name, path))
    }.toOption
  }

  /**
   * Deletes an Object from this S3 Bucket by name.
   *
   * @param objName the name of the object to delete from this S3 bucket
   * @return a string response
   */
  def delete(objName: String): String = {
    try {
      if (client.listObjects(name, objName).asScala.isEmpty) {
        "NOT FOUND"
      } else {
        client.removeObject(name, objName)
        "REMOVED"
      }
    } catch { case x: MinioException => x.getMessage }
  }

  private def test(testStr: String): String = testStr

  /**
   * Fetches the child directories of the given path for the S3Bucket.
   *
   * @param path the path of child directories to fetch
   * @return a list of full paths for the child directories
   */
  private def fetchChildDirs(path: String): List[String] = {
    try {
      if (!client.bucketExists(name)) return Nil
    } catch { case mEx: MinioException =>
      println("Error occurred: " + mEx)
      return Nil
    }

    Try {
      client.listObjects(name, s"$path/", false).asScala
        .flatMap(result => Try(result.get()).toOption)
        .filter(_.isDir)
        .map(_.objectName())
        .toList
    } getOrElse Nil
  }

  /**
   * Converts the given InputStream into a String
   *
   * @param is the InputStream to read into a String
   * @return the stringified contents of the InputStream
   */
  private def istream2str(is: InputStream): String = {
    val result = new ByteArrayOutputStream()
    val buffer = new Array[Byte](1024)
    var length: Int = is.read(buffer)

    while (length != -1) {
      result.write(buffer, 0, length)
      length = is.read(buffer)
    }

    result.toString("UTF-8")
  }
}
