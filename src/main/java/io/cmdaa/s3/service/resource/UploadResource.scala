package io.cmdaa.s3.service.resource

import java.io.InputStream
import java.lang.System.getProperty
import java.util.UUID.randomUUID
import com.google.gson.GsonBuilder
import io.cmdaa.data.util.OptionSerializer
import io.cmdaa.s3.service.auth.AppUser
import io.dropwizard.auth.Auth

import javax.ws.rs._
import javax.ws.rs.core.MediaType.{APPLICATION_JSON, TEXT_PLAIN}
import javax.ws.rs.core.{MediaType, Response}
import org.glassfish.jersey.media.multipart.{FormDataContentDisposition, FormDataParam}

@Path("/file")
class UploadResource(currentBucket: s3Bucket) {
  val logPattern =
    """([0-9a-zA-Z.\-_@ ]+)/([0-9a-zA-Z.\-_ ]+)_([0-9]+)/""".r

  lazy val gson = {
    val gBuilder = new GsonBuilder()
    gBuilder.registerTypeAdapter(classOf[Option[Any]], new OptionSerializer)
    gBuilder.create
  }

  @GET
  @Path("list")
  @Produces(Array(APPLICATION_JSON))
  def listFiles(@Auth principal: AppUser): Response = {
    val files = currentBucket
      .listUploads(principal.name)
      .flatMap { logPath => logPath match {
        case logPattern(username, logName, lastModifiedStr) =>
          val runData = currentBucket
            .listRuns(logPath)
            .map(fetchRunData(logPath))

          Some(Map (
            "path"         -> logPath,
            "username"     -> username,
            "logName"      -> logName,
            "lastModified" -> lastModifiedStr.toLong,
            "runs"         -> runData
          ))

        case _ =>
          println("LogPattern not found in: " + logPath)
          None
      }}

    Response.ok
      .entity(Map("results" -> files))
      .build()
  }

  @GET
  @Consumes(Array(APPLICATION_JSON))
  @Produces(Array(APPLICATION_JSON))
  @Path("output")
  def getOutput (
    @Auth principal: AppUser,
    @QueryParam("file") file: String
  ): Response = {
    val groks: List[GrokMessage] = currentBucket
      .fetch(file)
      .map {
        _.split("\n").toList
          .map(_.trim)
          .filter(_.nonEmpty)
          .map { line =>
            gson.fromJson(line, classOf[GrokMessage])
          }
      }
      .getOrElse(Nil)

    Response.ok
      .entity(Map("groks" -> groks))
      .build()
  }

  @GET
  @Produces(Array(TEXT_PLAIN))
  @Path("text")
  def getFile (
    @Auth principal: AppUser,
    @QueryParam("file") file: String
  ): Response = {
    currentBucket.fetch(file).fold {
      Response.noContent()
        .entity(s"Path: '$file', Bucket: '${currentBucket.name}', Msg: 'Not Found'")
        .build()
    } { contents =>
      Response.ok
        .entity(contents)
        .build()
    }
  }

  @POST
  @Path("upload")
  @Produces(Array(APPLICATION_JSON))
  @Consumes(Array(MediaType.MULTIPART_FORM_DATA))
  def uploadFile (
    @Auth principal: AppUser,
    @FormDataParam("file") uploadedInputStream: InputStream,
    @FormDataParam("file") fd: FormDataContentDisposition,
    @FormDataParam("lmDate") lmDate: Long
  ): Response = {
    /* Need code here to get a uuid for the file name
       Then return the uuid if we have success and of course 200 */

    val fileName = if (fd != null) {
      val fn = fd.getFileName.replaceAll(" ", "_")
      s"""${principal.name}/${fn}_$lmDate/log"""
    } else {
      s"${principal.name}/${randomUUID.toString}_$lmDate/log"
    }

    val retVal: String = currentBucket
      .upload(fileName, uploadedInputStream, overwrite = false)

    // retVal calls currentBucket and returns either the file name that was
    // uploaded or "" if no file was uploaded.

    if (retVal == "") {
      // Use fileName as the key value to pass the current name value to the
      // Response object.
      // See how this is done below for the response to the "test" operation
      Response.notModified
        .entity(Map("fileName" -> "", "bucket" -> currentBucket.name))
        .build()
    } else {
      Response.ok
        .entity(Map("fileName" -> fileName, "bucket" -> currentBucket.name))
        .build()
    }
  }

  @DELETE
  @Path("delete")
  @Produces(Array(APPLICATION_JSON))
  def delete (
    @Auth principal: AppUser,
    @QueryParam("file") file: String
  ): Response = {
    val retVal: String = currentBucket.delete(file)

    Response.ok
      .entity(Map(file -> retVal))
      .build()
  }

  @GET
  @Path("delete/{fileName}")
  @Produces(Array(APPLICATION_JSON))
  def deleteFile (
    @Auth principal: AppUser,
    @PathParam("fileName") fileName: String
  ): Response = {
    val retVal: String = currentBucket.delete(fileName)

    Response.ok
      .entity(Map(fileName -> retVal))
      .build()
  }

  @GET
  @Path("test")
  @Produces(Array(APPLICATION_JSON))
  def default(@Auth principal: AppUser): Response =
    Response.ok
      .entity(Map (
        "name"            -> "dropwizard-scala-example",
        "message"         -> "upload test get works",
        "java.version"    -> getProperty("java.version"),
        "java.vm.version" -> getProperty("java.vm.version")
      ))
      .build()

  private def fetchRunData(logPath: String)(runPath: String)
  : Map[String, Any] = {
    val runId = runPath
      .substring(logPath.length, runPath.length-1)
      .toLong

    val status = currentBucket
      .fetch(runPath+"/status.json")
      .map(s => gson.fromJson(s, classOf[RunStatus]))
      .getOrElse {
        RunStatus (
          isRunning  = true,
          isComplete = false,
          IsError    = false,
          message    = "Initializing Workflow..."
        )
      }

    Map("path" -> runPath, "runId" -> runId, "status" -> status)
  }
}

case class RunStatus (
  isRunning:  Boolean,
  isComplete: Boolean,
  IsError:    Boolean,
  message:    String
)

case class GrokMessage (
  grok: String,
  logMessages: Array[LogMessage]
)

case class LogMessage(logMessage: String)
