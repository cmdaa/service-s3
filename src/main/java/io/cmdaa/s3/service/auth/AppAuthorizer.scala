package io.cmdaa.s3.service.auth

import io.dropwizard.auth.Authorizer

class AppAuthorizer extends Authorizer[AppUser] {
  override def authorize(appUser: AppUser, role: String): Boolean = {
    appUser.roles.nonEmpty && appUser.roles.contains(role)
  }
}
