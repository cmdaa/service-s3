package io.cmdaa.s3.service.auth

import java.util.{Calendar, Optional, UUID}

import io.dropwizard.auth.Authenticator
import com.typesafe.scalalogging.Logger
import io.cmdaa.data.dao.UserProjectRepo

// TODO: Replace with new AuthService

class AppAuthenticator (
  repo: UserProjectRepo,
  tokenExpires: Long
) extends Authenticator[String, AppUser] {
  private val logger = Logger[AppAuthenticator]

  override def authenticate(accessTokenId: String): Optional[AppUser] = {
    var ret: Optional[AppUser] = Optional.empty[AppUser]

    try {
      // Check input, must be a valid UUID
      UUID.fromString(accessTokenId)

      // Get the access token from the database
      val tokenOption = repo.findAccessToken(accessTokenId)

      if (tokenOption.nonEmpty) {
        val token = tokenOption.get
        val nowMillis = Calendar.getInstance.getTime.getTime

        // Check if the last access time is not too far in the past (the access
        // token is expired)
        if ((nowMillis - token.lastAccessDateMillis) <= tokenExpires) {
          // Update the access time for the token
          repo.touchAccessToken(accessTokenId)

          // Return the user principal for processing
          val userOption = repo.findUser(token.userId)

          if (userOption.nonEmpty)
            return Optional.of (
              new AppUser (
                userOption.get.id.get,
                userOption.get.username,
                Set("ADMIN")
              )
            )
        } else {
          repo.deleteAccessToken(accessTokenId)
        }
      }
    } catch { case iaEx: IllegalArgumentException =>
      logger.info(s"Bad Format for UUID: $accessTokenId")
    }

    return Optional.empty[AppUser]
  }
}
