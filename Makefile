NAME	= cmdaa/cmdaa-service-s3
TAG	= 0.0.15

build:
	mvn -Drevision=$(TAG) clean install -DskipTests

image:
	-docker rmi $(NAME):$(TAG)
	docker build \
	  --build-arg JAR_FILE=service-$(TAG).jar  \
	  --tag $(NAME):$(TAG) \
            .
