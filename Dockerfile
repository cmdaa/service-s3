FROM openjdk:8-jre-alpine
MAINTAINER Matt D

WORKDIR /usr/share/cmdaa

ENTRYPOINT ["/usr/bin/java", "-jar", "/usr/share/cmdaa/cmdaa-s3-services.jar", "server", "/usr/share/cmdaa/config.yml"]

# Add Maven dependencies (not shaded into the artifact; Docker-cached)
# ADD target/lib           /usr/share/cmdaa/lib
# Add the service itself

ARG JAR_FILE
ADD target/${JAR_FILE} /usr/share/cmdaa/${JAR_FILE}
ADD target/cmdaa-s3-services.jar /usr/share/cmdaa/cmdaa-s3-services.jar
ADD keystore.jks /usr/share/cmdaa/keystore.jks
ADD truststore.jks /usr/share/cmdaa/truststore.jks

ADD target/classes/application.conf /usr/share/cmdaa/application.conf
ADD target/classes/config.yml /usr/share/cmdaa/config.yml
ADD target/classes/logback.xml /usr/share.cmdaa/logback.xml
